import java.util.Scanner;

public class Variaveis {

    public static void main(String[] args) {

        int produtos = 10;

        int quantidade = 15;

        double total = produtos * quantidade;

        String nome = "Alex Fernando";

        System.out.println(total);

        total = (produtos * quantidade) * 5;
        System.out.println(total);

        System.out.println("Digite a idade: ");
        Scanner input = new Scanner(System.in);
        int idade = input.nextInt();

        if (idade == 31) {
            System.out.println(nome);
        }

        if (idade == 33) {
            nome += " Egidio";
            System.out.println(nome);
        }
    }
}
