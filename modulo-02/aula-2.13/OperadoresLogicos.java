import java.util.Scanner;

public class OperadoresLogicos {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Digite true ou false: ");
        boolean numero1 = input.nextBoolean();

        System.out.println("Digite true ou false outra vez: ");
        boolean numero2 = input.nextBoolean();

        if (numero1 || numero2) {
            System.out.println("entrou na condição");
        }
    }
}
